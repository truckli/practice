# --coding: utf-8 --

import os
import codecs
import re

from datetime import datetime,timedelta

## today = datetime.strptime("20190415", "%Y%m%d") ## 在此处修改非默认日期
today = datetime.today() ## 默认产生当日报表
yesterday = today - timedelta(1)

currfile = "rawinputs/cac.gov.cn-%d.%d" %(today.month, today.day)
prevfile = "rawinputs/cac.gov.cn-%d.%d" %(yesterday.month, yesterday.day)

for fname in [prevfile, currfile]:
    os.system("pandoc -o %s.md %s.docx" %(fname, fname))


def parse_rawinput(fname):
    state = "init"
    hour_reqs = {}
    hour_cc = {}
    hour_attacks = {}
    for line in codecs.open(fname+".md", encoding="utf-8"):
        if line.find("目录") >= 0:
            state = "in_toc"
        elif line.find("#网站基本情况") >= 0:
            state = "in_chap2"
        elif line.find("#被黑客攻击的攻击详细情况") >= 0:
            state = "in_sec3-2"
            hour = 0
        elif line.find("#抗d保") >= 0:
            state = "in_chap4"

        if state == "in_chap2":
            m = re.search("([0-2][0-9]):00\s+(\d+)\s+", line)
            if m is not None:
                hour, hour_req = int(m.group(1)), int(m.group(2))
                hour_reqs[hour] = hour_req
        elif state == "in_sec3-2":
            if line.find("总计") >= 0: 
                state = "in_chap4"
                continue
            m = re.search("([0-2][0-9]):00\s+(\w+)\s+(\d+)", line)
            if m is not None:            
                hour, hour_item, hour_attacks_item = int(m.group(1)), m.group(2), int(m.group(3))
                hour_attacks[hour] = hour_attacks_item
                continue
            m = re.search("\s+(\w+)\s+(\d+)", line)
            if m is not None:
                hour_item, hour_attacks_item =  m.group(1), int(m.group(2))
                if hour_item.find("CC") >= 0:
                    hour_cc[hour] = hour_attacks_item
                hour_attacks.setdefault(hour, 0)
                hour_attacks[hour] += hour_attacks_item
                continue
        
    return hour_reqs, hour_attacks, hour_cc
      
day_reqs, day_attacks, day_cc = 0, 0, 0
peak_hour, peak_attacks = 0, 0

hour_reqs, hour_attacks, hour_cc = parse_rawinput(prevfile)
for hour in range(16, 24):
    hour_reqs.setdefault(hour, 0)
    hour_attacks.setdefault(hour, 0)
    hour_cc.setdefault(hour, 0)
    day_reqs += hour_reqs[hour]
    day_attacks += hour_attacks[hour]
    day_cc += hour_cc[hour]
    if hour_attacks[hour] > peak_attacks:
        peak_hour, peak_attacks = hour, hour_attacks[hour]
hour_reqs, hour_attacks, hour_cc = parse_rawinput(currfile)
for hour in range(0, 16):
    hour_reqs.setdefault(hour, 0)
    hour_attacks.setdefault(hour, 0)
    hour_cc.setdefault(hour, 0)
    day_reqs += hour_reqs[hour]
    day_attacks += hour_attacks[hour]
    day_cc += hour_cc[hour]
    if hour_attacks[hour] > peak_attacks:
        peak_hour, peak_attacks = hour, hour_attacks[hour]

        
report = ""
report += "请求量\t%d\n" % day_reqs
report += "攻击量\t%d\n" % day_attacks
report += "CC量\t%d\n" % day_cc
report += "攻击峰值\t%d\n" % peak_hour
report += "峰值攻击量\t%d\n" % peak_attacks

print(report)
with codecs.open("summary%d-%d.txt" % (today.month, today.day), "w") as f:
    f.write(report)